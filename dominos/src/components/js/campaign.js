import React, { Component } from 'react';
// import logo from './logo.svg';
import './../css/campaign.css';
import './../images/campaigning.png';

class Campaign extends Component {


  render() {

    const path = {
      backgroundImage: `url(${this.props.campaign.path})`
    };

    const title = this.props.campaign.title;


    return (
      

      <div className="campaign" >
          <div className="campaign-image" style={path}></div>
          <div className="campaign-title">{title}</div>
      </div>
    );
  }
}

export default Campaign;
