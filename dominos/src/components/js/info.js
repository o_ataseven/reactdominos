import React, { Component } from 'react';
// import logo from './logo.svg';
import './../css/info.css';

class Info extends Component {
  render() {
    return (
      <div className="info">
          <li>
            <label className="i-label">Ye Kazana Hoşgeldin.</label>
          </li>
          <li>
            <label className="i-label">Her 5 evlere servis siparişine 1 adet pizza kazan. Ayrıca hediyeler, özel kampanyalar ve özel günlerde süprizlerimizi kaçırma. </label>
          </li>
      </div>
    );
  }
}

export default Info;
