import React, { Component } from 'react';
// import logo from './logo.svg';
import './../css/collview.css';

class CollView extends Component {


  

  render() {


    const style = {
      backgroundImage: `url(${this.props.collview.imageUrl})`
    };



    let { id, name, title, imageUrl, price } = this.props.collview;

    const _price = this.props.collview.price;

 


    return (
      <div className="collview">
        <div className="c-image" style={style}>
          <label className="c-labeltitle">ORTA BOY PİZZA</label><br />
          <label className="c-labelprice">{_price}</label>
          <label className="c-labelTL">TL</label><br />
          <button  style={{'display': 'block'}} className="c-button">SİPARİŞ VER</button>
    
        </div>
      </div>
    );
  }
}

export default CollView;
