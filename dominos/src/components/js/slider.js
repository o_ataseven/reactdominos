import React, { Component } from 'react';
// import logo from './logo.svg';
import './../css/slider.css';



class Slider extends Component {

  render() {


    const sliderImage1Url = require("./../images/slider1.jpg");


    return (
      <div className="slider">
        <img className="s-img" src={sliderImage1Url}></img>
      </div>
    );
  }
}

export default Slider;
