import React, { Component } from 'react';
// import logo from './logo.svg';
import './App.css';
import Header from './components/js/header';
import Nav from './components/js/nav';
import Info from './components/js/info';
import Slider from './components/js/slider';
import Campaign from './components/js/campaign';
import CollView from './components/js/collview';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      campains: [],
      collviews: [],
      justClicked: null,
    };
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(e) {
    e.preventDefault()
  }

  

  componentDidMount() {

    const dataCampaign =
      [
        {
          "title": "Tüm Kampanyalar",
          "path": "https://image.flaticon.com/icons/svg/95/95389.svg",
        },
        {
          "title": "Tüm Pizzalar",
          "path": "https://image.flaticon.com/icons/svg/734/734691.svg",
        },
        {
          "title": "Ekstra Lezzetler",
          "path": "https://image.flaticon.com/icons/svg/135/135415.svg",
        },
      ]


    this.setState({
      campains: dataCampaign
    }
    );

    const data =
      [
        {
          "id": "1",
          "name": "pizza1",
          "title": "ORTA BOY PİZZA",
          "imageUrl": "http://www.pizzapizza.com.tr/static/636334687622656250.png",
          "price": "10.50"

        },
        {
          "id": "2",
          "name": "pizza2",
          "title": "MOBİL'DEN ORTA BOY",
          "imageUrl": "https://www.smilepizza.com.ua/images/cache/1239/1_1500.jpg",
          "price": "20.50"

        },
        {
          "id": "3",
          "name": "pizza3",
          "title": "2 ORTA BOY SUCUKLU",
          "imageUrl": "http://www.pizzapizza.com.tr/static/636334700542968750.png",
          "price": "30"

        },
        {
          "id": "4",
          "name": "pizza4",
          "title": "BÜYÜK BOY PİZZALAR",
          "imageUrl": "https://www.evrimselantropoloji.org/wp-content/uploads/2015/11/pizza-3.jpg",
          "price": "14.50"

        },
        {
          "id": "5",
          "name": "pizza5",
          "title": "BOL PEYNİRLİ EKMEK VE YOĞURTLU SOS",
          "imageUrl": "http://www.pizzapizza.com.tr/static/636334687622656250.png",
          "price": "35"

        },
        {
          "id": "6",
          "name": "pizza6",
          "title": "BÜYÜK BOY PİZZALAR",
          "imageUrl": "https://www.evrimselantropoloji.org/wp-content/uploads/2015/11/pizza-3.jpg",
          "price": "40"

        },
        {
          "id": "7",
          "name": "pizza7",
          "title": "ORTA BOY PİZZA",
          "imageUrl": "http://www.pizzapizza.com.tr/static/636334687622656250.png",
          "price": "12.50"

        },
        {
          "id": "8",
          "name": "pizza8",
          "title": "BOL PEYNİRLİ EKMEK VE YOĞURTLU SOS",
          "imageUrl": "https://www.smilepizza.com.ua/images/cache/1239/1_1500.jpg",
          "price": "30"

        }

      ]
    this.setState({
      collviews: data
    })
  }





  render() {
    return (
      <div className="App">
        <Header />
        <Nav />
        <Info />
        <Slider />

        <div className="campains">
          {this.state.campains.map(function (campaign) {
          
            return <Campaign
              key={campaign.title}
              campaign={campaign}
            />
          })
          }
        </div>

        <div className="collviews">
          {this.state.collviews.map(function (collview) {
            return <CollView
              
              key={collview.name}
              collview={collview}
            />
          })
          }
        </div>
      </div>
    );
  }
  
}


export default App;
